pragma solidity ^0.4.11;

contract Diary {
    
    event countAll(address owner, uint count);
    
    struct Entry {
        bytes32 uuid;
        bytes32 title;
        string text;
    }
    
    address owner;
    
    mapping(address => Entry[]) entryList;
    mapping(address => uint) entryCount;
    
    function Diary() {
        owner = msg.sender;
    }
    
    function addEntry(bytes32 title, string text) {
        bytes32 uuid = keccak256(title, text);
        var new_entry = Entry(uuid, title, text);
        entryList[msg.sender].push(new_entry);
        entryCount[msg.sender]++;
    }
    
    function countEntries() constant returns (uint count) {     
        return entryCount[msg.sender];
    }
    
    function removeEntry(uint id) {
        delete entryList[msg.sender][id];
        entryCount[msg.sender]--;
    }
    
    function getEntryByUUID(bytes32 uuid) constant returns (bytes32 title, string text) {
        for (uint i=0; i<entryCount[msg.sender]; i++) {
            if (entryList[msg.sender][i].uuid == uuid) {
                return (entryList[msg.sender][i].title, entryList[msg.sender][i].text);
            }
        }
    }
    
    function getEntryByIndex(uint idx) constant returns (bytes32 uuid, bytes32 title, string text) {
        if (idx < entryCount[msg.sender]) {
            return (entryList[msg.sender][idx].uuid, entryList[msg.sender][idx].title, entryList[msg.sender][idx].text);
        }
    }

}