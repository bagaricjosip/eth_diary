// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

/*
 * When you compile and deploy your Diary contract,
 * truffle stores the abi and deployed address in a json
 * file in the build directory. We will use this information
 * to setup a Diary abstraction. We will use this abstraction
 * later to create an instance of the Diary contract.
 */
import diary_artifacts from '../../build/contracts/Diary.json'

var Diary = contract(diary_artifacts);

window.saveEntry = function() {

  let title = $("#newEntryTitle").val();
  let text = $("#newEntryTextarea").val();

  try {
    $("#msg").html("You entry has been added.")
    $("#newEntryTitle").val("");
    $("#newEntryTextarea").val("");

    /* Diary.deployed() returns an instance of the contract. Every call
     * in Truffle returns a promise which is why we have used then()
     * everywhere we have a transaction call
     */
    Diary.deployed().then(function(contractInstance) {
      contractInstance.addEntry(title, text, {gas: 140000, from: web3.eth.accounts[0]}).then(function() {
        console.log("Added.");
        window.fetchEntries();
      });
    });
  } catch (err) {
    console.log(err);
  }
}

window.fetchEntries = function () {
  Diary.deployed().then(function(contractInstance) {
    contractInstance.countEntries({gas: 100000, from: web3.eth.accounts[0]}).then(function(data) {

      let totalEntries = parseInt(data.toString());
      $("#totalEntries").html(data.toString());
      let fetchEntriesNum = totalEntries;

      if (totalEntries > 5) {
        fetchEntriesNum = 5;
        $("#totalEntries").append(" (showing only first 5)")
      }

      $("#diaryList").html("");

      for (var i = 0; i < fetchEntriesNum; i++) {
        contractInstance.getEntryByIndex.call(i).then(function(data) {
          let uuid = web3.toAscii(data[0]);
          let title = web3.toAscii(data[1]);
          let text = data[2];
          $("#diaryList").append('<div id="' + uuid + '" class="card"><div class="card-body"><h4 class="card-title">' + title + '</h4><p class="card-text">' + text + '</p></div></div>');
        });
      }

    });
  });
}

$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  Diary.setProvider(web3.currentProvider);

  window.fetchEntries();
  
});